# ISMIP6 GrIS Configuration manager for ElmerIce

<img src="pictures/GrIS2.png" align="center" alt="Greenland Ice Sheet with Elmer/Ice">

## Description 

Configuration manager to perform standard ISMIP6 Greenland projections with [Elmer/Ice](http://elmerice.elmerfem.org/), see this [link](https://www.climate-cryosphere.org/wiki/index.php?title=ISMIP6-Projections-Greenland) for the description of the experimental protocol for the ISMIP6 projections.

Here a **configuration** designates one model set-up (initial state and parameters) from which we will run several **simulations**, e.g. an historical (hist) simulation, a control (ctl) simulation and one or more projections (proj).

### Configuration generic description

- Calendar : no leap 365d calendar  
- time unit system for Elmer is "days"  
- grid mapping : Polar Stereographic North (epsg:3413) 

- force balance : Shelfy-Stream Approximation
- no thermo-mecanical coupling
- basal melt under floating ice varies linearly between -20 m a-1 at a depth of -400m (and below) to 0 m a-1 at a depth of -100m (and above)
- [Xios](https://github.com/ElmerCSC/elmerfem/blob/elmerice/elmerice/Solvers/Documentation/XIOSOutputSolver.md) is used to output the results in the netcdf UGRID format

## Using this configuration manager

1. Prepare your database with the input files (see below for required inputs)

2. Prepare your **configuration**:
	- set the configuration parameters in Config\_parameters.bash 
	- run [MakeConfig.sh](MakeConfig.sh): this will create the configuration directory 

3. Prepare an run your **simulations**
	- go to the configuration directory
	- set the simulation parameters (see [Run\_templates](Run_templates) for examples)
	- run [MakeRun.sh](MakeRun.sh): this will create (and run) the simulation 
	- a simulation can be:
		- with constant forcing and no front retreat, e.g. for a relaxation and a ctrl simulation; see [ctrl\_src](ctrl_src)
		- an ISMIP6 projection with prescribed SMB anomaly and front retreat; see [proj\_src](proj_src)

4. Process the outputs to ISMIP6 format: 
	- see [Xios2ISMIP6](Xios2ISMIP6)

## Configuration data base structure

- [CONFIG\_DB]/$MESH\_NAME: Mesh directory must contain:
	- $MESH\_NAME: Elmer mesh directory (serial and partitionned)
	- mesh\_cdo\_grid.nc: the unstructured mesh descrition for cdo
	- mesh\_cellarea.nc : "true" element size area 

- [CONFIG\_DB]/FrontRetreats/[CONFIG\_NAME]\_[MESH\_NAME]: directory with the pre-processed front retreat masks on the Elmer mesh eleemnts (see [PreProcessing/Front](PreProcessing/Front) to generate these files from the input files).

- [CONFIG\_DB]/SMB\_FORCINGS/[MESH\_NAME]: the surface mass balance anomalies (aSMB) and vertical gradient of SMB (dSMBdz) conservatively interpolated on the Elmer mesh elements (see [PreProcessing/aSMB](PreProcessing/aSMB) to generate these files from the input files).

- [CONFIG\_DB]/ISMIP6\_GRIDS: ISMIP6 regular grid(s) for the interpolation

- [CONFIG\_DB]/GRID\_RETREAT/GrIS\_00600m\_grid.nc : Regular grid for the retreat mask interpolation 

OPTIONNAL:

- [CONFIG\_DB]/reference\_smb.nc:
	- reference surface mass balance on the Elmer mesh elements
	- set in Config\_parameters.bash

- [CONFIG\_DB]/init.nc: 
	- initial state file (e.g. from an inversion) with required input variables
	- set in Run\_templates/hist.bash

## Contains

- Configuration sources:  
	- [MakeConfig.sh](MakeConfig.sh): prepare the configuration directory  
	- [Config\_parameters.bash](Config_parameters.bash) : template config. parameter files   

- Simulation tools:  
	- [MakeRun.sh](MakeRun.sh): prepare a simulation directory  
	- [Run\_templates](Run_templates): template config. files  

- Simulations source files: 
	- [ctrl\_src](ctrl_src): simulation under constant forcings  
	- [proj\_src](proj_src): ISMIP6 projection run  
	- [sif\_parameters](sif_parameters): sif general parameters projection and parameters
	- [xios\_src](xios_src): xios config. files

- Elmer codes specific to this config.
	- [ELMER\_SRC](ELMER_SRC)

- Pre-Processing tools:
	- [Front](PreProcessing/Front): Forced front retreat pre-processing; interpolation to Elmer grids and remove isolated patches
	- [aSMB](PreProcessing/aSMB): Interpolation of aSMB and dSMBdz from the regular grid to the Elmer grid

- Post-processing tools: 
	- [Xios2ISMIP6](Xios2ISMIP6): 
		- Interpolation of the results to the regular ISMIP6 grid
		- Interpolation of the initial ice mask to the regular grid @600m to produce the front retreats
	- [PostProcessing](PostProcessing): codes for postprocessing


## TO DO

- Configuration improvements:  
	- [x] Projection aSMB, dSMBdz and retreat\_mask as parameters (in MakeRun.sh and proj\_src)
	- [ ] Add other friction laws (in MakeRun.sh)
		- [x] Weertman
		- [x] Reg. Coulomb
	- [ ] Set friction coefficient frc units in xios directly from Elmer instead of preprocessing in MakeRun.sh ?
	- [ ] Set global attributes (institution, contacts, titles) directly with xios
	- [ ] implement restart mechanism
	- [ ] add post-processing variables:    
		- [ ] driving stress  
		- [ ] strain-rates, (membrane ?) stresses
	- [ ] implement prognostic front retreat using regional retreat rates
	- [ ] interpolation on a regular grid directly from XIOS?
	- [ ] apply smb only on glaciated elements?

- Documentation:  
	- [x] Add Documentation for the scripts MakeConfig.sh and MakeRun.sh
	- [x] Add README.md in sub-directories

- Post-Processing tools:  
	- [x] Add interpolation tools: 
		- Work in dahu (and mahti?) if we extract the variables in netcdf classic files not netcdf4; implement a test or option? would it be better to work on netcdf4?
		- would be easier to do everything with cdo and not a mix nco cdo?
	- [x] interpolation of initial ice mask to regular 600m grid: idem.
	
	- [ ] Add generic tools to construct the data base
		- [ ] generate Elmer grid definition files (UGRID, CDO)
		- [x] interpolation to Elmer grid
		- [x] retreat masks pre-processing

	- [ ] plotting tools

	- [ ] remeshing and mesh2mesh interpolation

- Testing : 
	- [ ] : performance runs (scalabity)
	- [ ] : reproductibility (partitions; plateforms)
