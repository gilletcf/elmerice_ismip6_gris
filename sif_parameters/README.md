## Generic sif parameters

Generic files included in the simulation .sif files.

## Contains

- [proj.inc.sif](proj.inc.sif): the geographical projection, here the Greenland polar stereo-graphic projection epsg:3413
- [elmer_d.param.sif](elmer_d.param.sif ): generic physical (gravity, densities, ...) and model parameters (Glen index, critical thickness) used for all simulations
