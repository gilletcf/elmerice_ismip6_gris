#!/bin/bash
###############################################################################
#  Create a configuration directory
#
#  Mandatory parameters:
# 	- CONFIG_NAME: name of the configuration directory
#	- ELMER_GrIS_HOME: path to the source configuration codes
#
#  Optional parameters:
#    - for Elmer:
#	- CONFIG_DB: the path to the config database that contains the input files
#	- MESH_NAME: name of the mesh
#	- FRICTION: the friction law and,eventually, its parameters
#	- SMB_REF: the reference surface mass balance
#    - for the interpolation to the ISMIP6 grid:
#	- ISMIP6_GRID: the regular ISMIP6 grid to interpolate the results
#	- institution: the name of the institution running the model
#	- contacts: contact details
#
#  requires:
#	- Config_parameters.bash: input configuration parameters files
#
#  usage:
#	./MakeConfig.sh 
################################################################################

function param_exist() {
        if [ -z "$2" ] ; then
		echo "parameter $1 is not set"; rm -rf $CONFIG_NAME ; exit 42
	fi
}

function CheckMandatoryKW() {
        ## Check Mandatory Keywords

## CONFIG_NAME
	## check that CONFIG_NAME does not already exist
	if [ -z "$CONFIG_NAME" ]; then
		echo "CONFIG_NAME not set; exit."; exit 42
	fi
	if [ -d "$CONFIG_NAME" ]
	then
      		echo "dir. $CONFIG_NAME already exist; exit" ; exit 42
	fi
 
## ELMER_GrIS_HOME
	## check that source repo exist
	if [ -z "$ELMER_GrIS_HOME" ]; then
		echo "ELMER_GrIS_HOME not set; exit"; exit 42
	fi

	SRC_DIR=$ELMER_GrIS_HOME
	if [ ! -d $SRC_DIR ]; then
		echo "dir. $ELMER_GrIS_HOME does not exist; exit" ; exit 42
	fi
	CONFIG_rev=$(cd $SRC_DIR; git rev-parse --short  HEAD)
}

## source config. parameter file
source Config_parameters.bash || exit 42

## Check that mandatory KW are defined
CheckMandatoryKW

echo "CREATE $CONFIG_NAME configuration directory"

## Create config. directory
mkdir -p $CONFIG_NAME || exit 42

## cp the config. def. file to the Config. directory
echo "#!/bin/bash" > $CONFIG_NAME/Config_parameters.bash || exit 42

echo "# Mandatory Parameters:" >> $CONFIG_NAME/Config_parameters.bash || exit 42
echo "## ELMER GriS SRC" >> $CONFIG_NAME/Config_parameters.bash || exit 42
echo "ELMER_GrIS_HOME=\"$SRC_DIR\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
echo "ELMER_GrIS_Rev=\"$CONFIG_rev\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42

echo "## CONFIG_NAME" >> $CONFIG_NAME/Config_parameters.bash || exit 42
echo "CONFIG_NAME=\"$CONFIG_NAME\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
echo "CONFIG_DIR=\"$PWD/$CONFIG_NAME\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42

echo >> $CONFIG_NAME/Config_parameters.bash || exit 42
echo "# Optionnal Parameters:" >> $CONFIG_NAME/Config_parameters.bash || exit 42
if [ ! -z $MESH_NAME ] ; then
	echo "MESH_NAME=\"$MESH_NAME\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
else
	echo "#MESH_NAME=unset" >> $CONFIG_NAME/Config_parameters.bash || exit 42
fi
if [ ! -z $SMB_REF ] ; then
	echo "SMB_REF=\"$SMB_REF\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
else
	echo "#SMB_REF=unset" >> $CONFIG_NAME/Config_parameters.bash || exit 42
fi
if [ ! -z "$FRICTION" ] ; then
	echo "FRICTION=\"$FRICTION\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
	FRICTION_lc=$(echo "$FRICTION" | tr '[:upper:]' '[:lower:]')
	case "$FRICTION_lc" in
	        linear)
			;;
		weertman)
			param_exist friction_exp $friction_exp
			echo "friction_exp=\"$friction_exp\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
			param_exist linear_velocity $linear_velocity
			echo "linear_velocity=\"$linear_velocity\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
			;;
		"regularized coulomb")
			param_exist friction_exp $friction_exp
			echo "friction_exp=\"$friction_exp\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
			param_exist linear_velocity $linear_velocity
			echo "linear_velocity=\"$linear_velocity\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
			param_exist u0 $u0
			echo "u0=\"$u0\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
			;;
			
		*)  echo "Sorry Friction Law not ready; exit"; rm -rf $CONFIG_NAME ; exit 42
	esac
else
	echo "#FRICTION=unset" >> $CONFIG_NAME/Config_parameters.bash || exit 42
fi
echo >> $CONFIG_NAME/Config_parameters.bash || exit 42

echo "## ISMIP6 ouputs keywords" >> $CONFIG_NAME/Config_parameters.bash || exit 42
vars=("ISMIP6_GRID" "institution" "contacts")
for var in "${vars[@]}" ; do
        if [ -n "${!var}" ]; then
                echo "$var=\"${!var}\"" >> $CONFIG_NAME/Config_parameters.bash || exit 42
        else
                echo "#$var=unset" >> $CONFIG_NAME/Config_parameters.bash || exit 42
        fi
done

echo "----------------------------------------------------"
echo "Config. parameters :"
cat $CONFIG_NAME/Config_parameters.bash
echo "----------------------------------------------------"

cd $CONFIG_NAME

## copy config files
mkdir -p SRC || exit 42

cp -r ${SRC_DIR}/sif_parameters SRC/. || exit 42
cp -r ${SRC_DIR}/Run_templates SRC/. || exit 42
cp ${SRC_DIR}/MakeRun.sh . || exit 42

## copy src directoryies
cp -r ${SRC_DIR}/*_src SRC/. || exit 42

## Create symbolik lonk to the config. data base
if [ -z "$CONFIG_DB" ]; then
        echo "Variable  CONFIG_DB is unset;"
	echo "  you will need to set symbolik link to your config. data_base"
	echo "  run ln -s $CONFIG_DB CONFIG_DB"
else
	if [ ! -d "$CONFIG_DB" ]; then
		echo "CONFIG_DB is set but not pointing to a directory"
		echo "  you will need to set symbolik link to your config. data_base"
		echo "  run ln -s $CONFIG_DB CONFIG_DB"
	else
		echo "create symbolik link to the config. data base"
		ln -s $CONFIG_DB CONFIG_DB || exit 42
		readlink CONFIG_DB
	fi
fi


