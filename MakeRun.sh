#!/bin/bash
###############################################################################
#  Create a simulation directory; copy the required input files and parameters and eventually run the Elmer simulation
#
#   this script first source Config_parameters.bash and thus inheritates the configuration parameters,
#   however they can be overwritten if also set in the simulation parameters file provided as argument
#
# Mandatory parameters:
#
# 1. Parameters implemented in parameters.sif:
#	- sim_name: the name of the simulation
#	- sim_type: the simulation type
#	- CONFIG_NAME: name of the configuration (usually inherited from Config_parameters.bash)
#	- MESH_NAME: name of the mesh (usually inherited from Config_parameters.bash)
#	- dt: the time step size in days
#	- ndt: the number of time steps
#	- SMB_REF: the reference surface mass balance
#	- start_date: the starting date for Xios
#	- restart_file: the file that contain the initial state of the simulation in netcdf UGRD format
#	- For projections:
#		- ZS_REF_FILE: the file that contain the reference surface elevation for the SMB/elevation feedback (in principle should be the same as the restart_file)
#		- R_FILE: the front retreat masks on the Elmer mesh elements
#		- aSMB_FILE: the SMB anomaly file on the Elmer mesh elements
#		- dSMBdz_FILE: the SMB elevtaion gradient file on the Elmer mesh elements
#
# 2. Parameters implemented in Friction_Material.sif
#	- FRICTION: The friction law definition (usually inherited from Config_parameters.bash)
#	- and associated parameters
#
# 3. a bash function called run_elmer, that implements how to run Elmer on your plateform
#
#  function run_elmer() {
#        ## define here how to run elmer...
#        ## eventually copy scheduler template files and submit the job or
#        # mpirun -np "N" ElmerSolver_mpi  with "N" the number of partitions
#  }
#
#  usage:
#       - as a script with the parameters set in a file: ./MakeRun.sh [sim_params.bash]
#		- see e.g.  hist.bash, ctrl.bash or  proj.bash under Run_templates/
#       - called from another script that set the inpus parameters: source MakeRun.sh
#		- see MakeProjs.sh under Run_templates to run a set of projections with different forcings but same parameters
#
################################################################################

function param_exist() {
	if [ -z "$2" ] ; then
		echo "parameter $1 is not set"; rm -rf $sim_name; exit 42
	fi
}

## source the generic configuration parameters
## source them first so they can be overwritten by the simulation parameters eventually...
source Config_parameters.bash || exit 42

## source argument file; if provided  as argument
if [ $# -eq 1 ]
then
	arg_file=$1
	if [ ! -f $arg_file ]; then
        	echo "argument is not an existint file; exit"; exit 42
	fi

	source $arg_file || exit 42
fi

## test if sim_name exist
if [ -z $sim_name ]; then
	echo "variable <sim_name> is not defined; exit"; exit 42
fi

## create simulation directory:
if [  -d $sim_name ]; then
	echo "simulation directory already exist; exit"; 

	# exit or return depending if the scirpt has been sourced or exectuted
	if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
	        exit 42
	else
	        return 42
	fi
else
	mkdir -p $sim_name || exit 42
fi

## make dir for INPUT FILES and create symbolic links as full names might bee too long?
mkdir -p $sim_name/INPUT_FILES || exit 42

## prepare the simulation files:
## set local parameters file
echo "! elmer input parameters" > $sim_name/parameters.sif

param_exist CONFIG_NAME $CONFIG_NAME
echo "\$config_name=\"$CONFIG_NAME\"" >> $sim_name/parameters.sif

echo "!   simulation names and id : " >> $sim_name/parameters.sif
echo "\$name=\"$sim_name\"" >> $sim_name/parameters.sif
echo "\$id=\"1\"" >> $sim_name/parameters.sif

param_exist MESH_NAME $MESH_NAME
echo "!   mesh directory name : " >> $sim_name/parameters.sif
echo "\$MESH_NAME=\"$MESH_NAME\"" >> $sim_name/parameters.sif

echo "!   time stepping parameters : " >> $sim_name/parameters.sif
param_exist dt $dt
param_exist ndt $ndt
echo "\$dt=$dt" >> $sim_name/parameters.sif
echo "\$ndt=$ndt" >> $sim_name/parameters.sif

param_exist SMB_REF $SMB_REF
fname="INPUT_FILES/"$(basename $SMB_REF)
if [ ! -f $sim_name/$fname ]; then
	ln -s $SMB_REF $sim_name/$fname || exit 42
fi
echo "!   reference SMB : " >> $sim_name/parameters.sif
echo "\$smb_ref_file=\"$fname\"" >> $sim_name/parameters.sif

param_exist start_date $start_date
echo "!   xios reference date : " >> $sim_name/parameters.sif
echo "\$reference_date=\"$start_date\"" >> $sim_name/parameters.sif

param_exist restart_file $restart_file
fname="INPUT_FILES/"$(basename $restart_file)
if [ ! -f $sim_name/$fname ]; then
	ln -s $restart_file $sim_name/$fname || exit 42
fi
echo "!   restart file  :  " >> $sim_name/parameters.sif
echo "\$restart_file=\"$fname\"" >> $sim_name/parameters.sif


## copy .sif files for the requires simulation type
##  add compilation ?
case "$sim_type" in
	ctrl) echo "  - control simulation"
		cp SRC/ctrl_src/* $sim_name/. || exit 42
		;;
	proj) echo "  - projection simulation"
		cp SRC/proj_src/* $sim_name/. || exit 42

		# simulations specific parameters
		# REF Zs for SMB/Elevation feedback might be different from the restart file if we implement restarts
		param_exist ZS_REF_FILE $ZS_REF_FILE
		fname="INPUT_FILES/"$(basename $ZS_REF_FILE)
		if [ ! -f $sim_name/$fname ]; then
			ln -s $ZS_REF_FILE $sim_name/$fname || exit 42
		fi
		echo "! zs_ref file for SMB/elevation feedback  : " >> $sim_name/parameters.sif
		echo "\$zs_ref_file=\"$fname\"">> $sim_name/parameters.sif

		param_exist R_FILE $R_FILE
		fname="INPUT_FILES/"$(basename $R_FILE)
		if [ ! -f $sim_name/$fname ]; then
			ln -s $R_FILE $sim_name/$fname || exit 42
		fi
		echo "! front retreat file  : " >> $sim_name/parameters.sif
	        echo "\$retreat_file=\"$fname\"">> $sim_name/parameters.sif

		param_exist aSMB_FILE $aSMB_FILE
		fname="INPUT_FILES/"$(basename $aSMB_FILE)
		if [ ! -f $sim_name/$fname ]; then
			ln -s $aSMB_FILE $sim_name/$fname || exit 42
		fi
		echo "! smb anomaly  : " >> $sim_name/parameters.sif
	        echo "\$aSMB_file=\"$fname\"">> $sim_name/parameters.sif

		param_exist dSMBdz_FILE $dSMBdz_FILE
		fname="INPUT_FILES/"$(basename $dSMBdz_FILE)
		if [ ! -f $sim_name/$fname ]; then
			ln -s $dSMBdz_FILE $sim_name/$fname || exit 42
		fi
		echo "! dSMBdz  : " >> $sim_name/parameters.sif
	        echo "\$dSMBdz_file=\"$fname\"">> $sim_name/parameters.sif
		;;
	*)     echo "unknown simuation type; exit" ; exit 42
esac

echo "----------------------------------------------------"
echo "local input parameters :"
cat $sim_name/parameters.sif
echo "----------------------------------------------------"


## create Friction_Material
## friction is converted to lower case
FRICTION_lc=$(echo "$FRICTION" | tr '[:upper:]' '[:lower:]')
case "$FRICTION_lc" in
        linear) 
                echo "SSA Friction Law = String \"linear\"" > $sim_name/Friction_Material.sif || exit 42
                echo "SSA Friction Parameter = Equals frC" >> $sim_name/Friction_Material.sif || exit 42
                XIOS_frc_def="\<field id=\"frc\"  long_name=\"ssa_linear_friction_coefficent\" unit=\"MPa m-1 d\"    grid_ref=\"GridNodes\" \/\>" 
                ;;
	weertman) 
		echo "SSA Friction Law = String \"Weertman\"" > $sim_name/Friction_Material.sif || exit 42
		echo "SSA Friction Parameter = Equals frC" >> $sim_name/Friction_Material.sif || exit 42
		param_exist friction_exp $friction_exp
		echo "SSA Friction Exponent = Real $friction_exp" >> $sim_name/Friction_Material.sif || exit 42
		param_exist linear_velocity $linear_velocity
		echo "SSA Friction Linear Velocity = Real $linear_velocity" >> $sim_name/Friction_Material.sif || exit 42
		parsed_exp=$(echo $friction_exp | sed 's/[$# ]//g;s@/@\\/@g' )
		XIOS_frc_def="\<field id=\"frc\"  long_name=\"ssa_weertman_friction_coefficent\" unit=\"MPa m^-$parsed_exp d^$parsed_exp\"    grid_ref=\"GridNodes\" \/\>"
		;;
	
	"regularized coulomb")
		echo "SSA Friction Law = String \"Regularized coulomb\"" > $sim_name/Friction_Material.sif || exit 42
		echo "SSA Friction Parameter = Equals frC" >> $sim_name/Friction_Material.sif || exit 42
		param_exist friction_exp $friction_exp
                echo "SSA Friction Exponent = Real $friction_exp" >> $sim_name/Friction_Material.sif || exit 42
                param_exist linear_velocity $linear_velocity
                echo "SSA Friction Linear Velocity = Real $linear_velocity" >> $sim_name/Friction_Material.sif || exit 42
                XIOS_frc_def="\<field id=\"frc\"  long_name=\"ssa_RegCoulomb_friction_coefficent\" unit=\"MPa\"    grid_ref=\"GridNodes\" \/\>"
                param_exist u0 $u0
                echo "SSA Friction Threshold Velocity=Real $u0" >> $sim_name/Friction_Material.sif  || exit 42
                ;;

        *)  echo "Sorry Friction Law not ready; exit"; exit 42
esac

echo "----------------------------------------------------"
echo "Friction law definition :"
cat $sim_name/Friction_Material.sif
echo "----------------------------------------------------"

## copy config. files to the simulation directory
cp SRC/sif_parameters/*.sif $sim_name/. || exit 42
cp SRC/xios_src/file_def_elmer.xml $sim_name/. || exit 42
cp SRC/xios_src/iodef.xml $sim_name/. || exit 42
sed "s/<FRICTION_FIELD>/$XIOS_frc_def/g" SRC/xios_src/context_elmer_d.xml > $sim_name/context_elmer_d.xml || exit 42

## copy the mesh ; should we get the realpath in case there is a symbolik link??
## check that there is the cell_area file??
cp -r CONFIG_DB/$MESH_NAME/$MESH_NAME $sim_name/.  || exit 42

## finally cd to sim_name
cd $sim_name

## compile required USFs and Solvers
export SRC_DIR=$ELMER_GrIS_HOME/ELMER_SRC
make -f $SRC_DIR/Makefile $sim_type

## and run ELMER
run_elmer

## come back up
cd ..
