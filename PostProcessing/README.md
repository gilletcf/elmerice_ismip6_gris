## Generic post-processing tools

Generic tools for post-processing

## Contains

- [Compute\_ismip6\_states\_anomalies.sh](Compute_ismip6_states_anomalies.sh): compute anomalies from an ismip6 state file; i.e. substract the values corresponding to the first time index
- [Compute\_ismip6\_states\_diff.sh](Compute_ismip6_states_diff.sh): compute the diffrence between two ismip6 state files.
