#!/bin/bash

input_file1=$1
input_file2=$2

output_file=diff.nc

## list of var to process
vars=("sftgif" "sftgrf" "sftflf" "strbasemag" "velmean" "lithk")
var_list=$(IFS=,; echo "${vars[*]}")

## the mesh description that needs to be passed
mesh_list="mesh2D,mesh2D_node_x,mesh2D_node_y,mesh2D_face_nodes,x,y"


echo "######################################################"
echo "# -- compute differences of variables:" $var_list
echo "# -- from files: " $input_file1 $input_file2
echo "# -- to file : " $output_file
echo "######################################################"

for v in ${vars[@]}
do
	# test var exist .. better test?
	tt=$(ncks -C -m -v $v $input_file1)
	retVal=$?
	if [ $retVal -ne 0 ]; then
	   echo "variable " $v "does not exist..."
	   exit 1
	fi
 
done

## compute differences
ncdiff -O -v $var_list $input_file1 $input_file2 $output_file

## copy mesh description
ncks -A -v $mesh_list $input_file1 $output_file

for v in ${vars[@]}
do
  ncrename -v $v,d_$v $output_file
done

