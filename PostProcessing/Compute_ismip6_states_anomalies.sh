#!/bin/bash

input_file=$1

if [ ! -f "$input_file" ]; then
    echo "provide an existing ismip6_state file as argument"
    exit 1
fi

output_file=${input_file%.*}_anomalies.nc

## list of var to process
vars=("sftgif" "sftgrf" "sftflf" "strbasemag" "velmean" "lithk")
var_list=$(IFS=,; echo "${vars[*]}")

## the mesh description that needs to be passed
mesh_list="mesh2D,mesh2D_node_x,mesh2D_node_y,mesh2D_face_nodes,x,y"


echo "######################################################"
echo "# -- compute anomalies of variables:" $var_list
echo "# -- from file: " $input_file
echo "# -- to file : " $output_file
echo "######################################################"

for v in ${vars[@]}
do
	# test var exist .. better test?
	tt=$(ncks -C -m -v $v $input_file)
	retVal=$?
	if [ $retVal -ne 0 ]; then
	   echo "variable " $v "does not exist..."
	   exit 1
	fi
 
done

## extract first time step
ncks -O  -F -d time,1 -v $var_list $input_file foo.nc
ncwa -O -C -a time foo.nc foob.nc

## compute anomalies
ncdiff -O $input_file foob.nc $output_file

## copy mesh description
ncks -A -v $mesh_list $input_file $output_file

## rename vars
for v in ${vars[@]}
do
  ncrename -v $v,d_$v $output_file
done

rm -f foo.nc foob.nc
