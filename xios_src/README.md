## Generic xios configuration files

Generic xios configuration files

> :warning: context\_elmer\_d.xml must be processed to replace **<FRICTION\_FIELD>** with the correct definition of the friction coefficient *frC*; This is done by [MakeRun.sh](../MakeRun.sh)

## Contains


- [iodef.xml](iodef.xml): xios simulation section and xios context
- [context\_elmer\_d.xml](context_elmer_d.xml): the elmerice context with the definition of the fields and grids
- [file\_def\_elmer.xml](file_def_elmer.xml): the file definition
