#!/bin/bash

## SMB unit conversion and remaping
#SMB [m d-1 ] = SMB [kg m-2 s-1] * {s d-1} / rho_i [kg m-3]
rhoi=910.0
sec_per_day=86400

######################################################################################################
# MAIN
######################################################################################################
while getopts 'd:m:' opt; do
  case "$opt" in
    d) DB_DIR="$OPTARG" ;;
    m) MESH_NAME="$OPTARG" ;;
    ?)
            echo "argument error; exit" 
            exit 42
            ;;
  esac
done

if [ -z "$DB_DIR" ];then
        echo "missing mandatory argument -d; exit"; exit 42
fi
if [ -z "$MESH_NAME" ];then
        echo "missing mandatory argument -m; exit"; exit 42
fi


## INPUT DIRECTORY WITH aSMB and dSMB on regular 1km grid
INPUT_DIR=${DB_DIR}/SMB_FORCINGS/GrIS_01000m

## OUTPUT  DIRECTORY WITH FRONT RETREATS ON MESH GRID
OUTPUT_DIR=${DB_DIR}/SMB_FORCINGS/${MESH_NAME}
mkdir -p $OUTPUT_DIR

## WORKING DIRECTORY
WRKDIR=./TMPDIR_dsmb

## input files for conservative interpolation
ELMER_GRIDFILE=${DB_DIR}/${MESH_NAME}/mesh_cdo_grid.nc
if [  ! -f "$ELMER_GRIDFILE" ]; then
        echo "elmer grid description file ${ELMER_GRIDFILE} does not exist; exit"; exit 42
fi
## regular grid file
GRID_FILE=${DB_DIR}/GRID_ISMIP6/ISMIP6_GrIS_1000m_grid.nc
if [  ! -f "$GRID_FILE" ]; then
        echo "GrIS_00600m_grid ${GRID_FILE} does not exist; exit"; exit 42
fi

######################################################################################################
## be carefull mesh might not be covered entirely so update _FillValue with -0.03 m d-1 (~-11m/a)
var_name="dSMBdz"
default=0.0

anomalies=$(ls ${INPUT_DIR}/${var_name}_*.tgz)

for an in $anomalies 
do
	rm -rf $WRKDIR ; mkdir $WRKDIR; 
	cd $WRKDIR

	fname=$(basename $an) 
	interp_file=${OUTPUT_DIR}/${fname%.*}_elmer.nc

	echo "############################################################"
	echo "############################################################"
	echo "## processing " $fname
	echo "## To  " $interp_file
	echo "############################################################"
        echo "############################################################"

	if [ -f $interp_file ]; then
		echo "$interp_file already exist cycle;"
		continue
	fi

        ## 1. extract files
	echo " - extract files..."
	tar xzf $an --wildcards *{2015..2100}.nc

	## 2. recompute weights from first file
	echo " - make weights..."
        file=$(ls ${var_name}/*2015.nc)
        cdo genycon,$ELMER_GRIDFILE -setgrid,$GRID_FILE $file weights.nc || exit 42

	## 3. interpolate
	mkdir -p tmp
	for i in {2015..2100}
	do
		file=$(ls ${var_name}/*$i.nc)
		if [ ! -f "$file" ]; then
		  echo "$file does not exist."
		  continue
   		fi
	        end_date=$i
		output_file=tmp/$i.nc
		echo "- interpolate $i"
		## -setmissval,-0.03 apply the correct value for the attributes but not in the file which has NaN... issue with used cdo version?
		cdo remap,$ELMER_GRIDFILE,weights.nc -setgrid,$GRID_FILE -selname,${var_name}_d -expr,"${var_name}_d=${var_name}*$sec_per_day/$rhoi" $file $output_file || exit 42
	done

	## 4. concatenate
	file_list=$(eval echo "tmp/{2015.."$end_date"}.nc")
	ncrcat -O $file_list $interp_file || exit 42
	## update attribute

	## update _FillValue; make it NaN so it can be detected...
	ncatted -a _FillValue,${var_name}_d,o,f,NaN $interp_file || exit 42
	## change it to a given val
	ncatted -a _FillValue,${var_name}_d,o,f,-0.03 $interp_file || exit 42
        ##
	ncatted -a unit,${var_name}_d,c,c,"m d-1 m-1" -a missing_value,${var_name}_d,d,,,  $interp_file || exit 42

	echo "## DONE ##"

	cd .. ; rm -rf $WRKDIR
done

