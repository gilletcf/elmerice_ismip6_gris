#!/bin/bash

######################################################################################################
# FUNCTIONS
######################################################################################################
# ncdmnsz $dmn_nm $fl_nm : What is dimension size?
function ncdmnsz { ncks --trd -m -M ${2} | grep -E -i ": ${1}, size =" | cut -f 7 -d ' ' | uniq ; }

### Compute icemask anomalies
###
Anomalies () {
  #Extract first time step 
  ncks -F -d time,1,1 $1 t1.nc || exit 42
  # remove time dimension
  ncwa -O -a time t1.nc t1.nc || exit 42
  #compute anomaly
  ncdiff -F -d time,2,-1 -v sftgif,time,x,y $1 t1.nc anomalies_reg.nc || exit 42
  #rm tmp file
  rm -rf t1.nc || exit 42
}

### Conservative interpolation to Elmer mesh
###
AnomaliesToElmer () {
	cdo remap,${ELMER_GRIDFILE},${WEIGHTS} -setgrid,${GRID_FILE} anomalies_reg.nc anomalies_elmer.nc
}

######################################################################################################
# MAIN
######################################################################################################
source Config_parameters.bash

## INPUT DIRECTORY WITH FRONT RETREATS ON REGULAR GRID
INPUT_DIR=${CONFIG_DIR}/CONFIG_DB/FrontRetreats/${CONFIG_NAME}
Forcings=$(ls $INPUT_DIR/retreatmasks*.nc)

## OUTPUT  DIRECTORY WITH FRONT RETREATS ON MESH GRID
OUTPUT_DIR=${CONFIG_DIR}/CONFIG_DB/FrontRetreats/${CONFIG_NAME}_${MESH_NAME}
mkdir -p $OUTPUT_DIR

## WORKING DIRECTORY
WRKDIR=$CONFIG_DIR/FrontPreProcessing

## input files for conservative interpolation
ELMER_GRIDFILE=${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/mesh_cdo_grid.nc
if [  ! -f "$ELMER_GRIDFILE" ]; then
        echo "elmer grid description file ${ELMER_GRIDFILE} does not exist; exit"; exit 42
fi
GRID_FILE=${CONFIG_DIR}/CONFIG_DB/GRID_RETREAT/GrIS_00600m_grid.nc
if [  ! -f "$GRID_FILE" ]; then
	echo "GrIS_00600m_grid ${GRID_FILE} does not exist; exit"; exit 42
fi

WEIGHTS=${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/toGrIS00600m/ycon_weights_GrIS_00600m_to_mesh_fracarea.nc
if [  ! -f "$WEIGHTS" ]; then
	echo "interpolation weights ${WEIGHTS} does not exist; create it"
	mkdir -p ${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/toGrIS00600m || exit 42
	cdo genycon,$ELMER_GRIDFILE $GRID_FILE $WEIGHTS || exit 42
fi

## go for it
echo "###################################################################################################"
echo "###################################################################################################"
echo "INPUT DIRECTORY: " $INPUT_DIR
echo "OUTPUT DIRECTORY: " $OUTPUT_DIR
echo "WORKING DIRECTORY: " $WRKDIR
echo "###################################################################################################"
echo "###################################################################################################"

## create working directory
mkdir -p $WRKDIR ; cd $WRKDIR


## go through the input files 
for ff in $Forcings
do

  ## check that input file exist
  INPUT_FILE=$ff
  if [  ! -f $INPUT_FILE ]; then
  	echo "input file ${INPUT_FILE} does not exist; exit"; exit 42
  fi

  ## extract case name
  NAME=$(basename -- ${INPUT_FILE%.nc})
  case=$(grep -oP 'retreatmasks_v1_\K.*?(?=_PRO)' <<< "$NAME")

  echo "###################################################################################################"
  echo "###   processing case:" $case
  echo "###################################################################################################"

  ## the output file
  OUTPUT_FILE=${OUTPUT_DIR}/${case}_elmer.nc
  ## check if already done
  if [ -f $OUTPUT_FILE ]; then
  	echo "output file already exist; continue"
	continue
  fi

  ## go to case dir
  rm -rf $case ; mkdir $case ; cd $case

  ## Compute anomalies
  Anomalies $INPUT_FILE
  AnomaliesToElmer

  ## get number of time steps to process; should be the size of the time dimension - 1
  ndt=$(ncdmnsz time anomalies_elmer.nc)

  ## compile required USFs and Solvers
  export SRC_DIR=${ELMER_GrIS_HOME}/ELMER_SRC
  make -s -f $SRC_DIR/Makefile FrontPreProcessing

  ## copy MESH
  cp -r ${CONFIG_DIR}/CONFIG_DB/$MESH_NAME/$MESH_NAME . || exit 42

  ## copy sif file
  cp ${ELMER_GrIS_HOME}/PreProcessing/Front/PreProcessing.sif . || exit 42
  
  ## set local parameters file
  echo "! elmer input parameters" > parameters.sif
  echo "!   mesh directory name : " >> parameters.sif
  echo "\$MESH_NAME=\"$MESH_NAME\"" >> parameters.sif
  echo "!   time stepping parameters : " >> parameters.sif
  echo "\$ndt=$ndt" >> parameters.sif

  ## run pre-processing
  ElmerSolver PreProcessing.sif 

  ## store output file
  cp passive.nc $OUTPUT_FILE  || exit 42

  cd $WRKDIR

  ## pack pre-processing stuff
  tar czf $case.tar.gz $case/anomalies_elmer.nc $case/passive.nc $case/*.sif $case/$MESH_NAME/*vtu $case/*.txt* || exit 42
  rm -rf $case || exit 42

done

