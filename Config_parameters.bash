#!/bin/bash

## Mandatory Keywords
ELMER_GrIS_HOME=$ELMER_GrIS_HOME

CONFIG_NAME="TEST"

## Optionnal Keywords
CONFIG_DB="$PWD/${CONFIG_NAME}_DB"

MESH_NAME="MESH_5"

SMB_REF="${CONFIG_DB}/reference_smb.nc"

FRICTION="Linear"

# FRICTION="Weertman"
# friction_exp="$ 1.0/3.0"
# linear_velocity="1.0e-4" 

## ISMIP6 ouputs keywords
ISMIP6_GRID="ISMIP6_GrIS_5000m"
institution="Institut des G\'eosciences de l'Environnement, CNRS, Grenoble, France"
contacts="F. Gillet-Chaulet, E. Jager"
