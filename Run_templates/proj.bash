#!/bin/bash

RCM="MARv3.12"
GCM="ACCESS1.3-rcp85"
R="Rhigh"

sim_name=${RCM}_${GCM}-${R}

# simulation type; constant forcing
sim_type="proj"

# restart file; e.g. restart the hist run.
restart_file="${CONFIG_DIR}/hist/restart_hist_1.nc"

# the reference elevation for smb elavation feedbaack
ZS_REF_FILE="${CONFIG_DIR}/hist/restart_hist_1.nc"

# SMB anomaly
aSMB_FILE="${CONFIG_DIR}/CONFIG_DB/SMB_FORCINGS/${MESH_NAME}/aSMB_${GCM}_${RCM}_elmer.nc"
# dSMBdz
dSMBdz_FILE="${CONFIG_DIR}/CONFIG_DB/SMB_FORCINGS/${MESH_NAME}/dSMBdz_${GCM}_${RCM}_elmer.nc"
# Reterat File
R_FILE="${CONFIG_DIR}/CONFIG_DB/FrontRetreats/${CONFIG_NAME}_${MESH_NAME}/${sim_name}_elmer.nc"

# simulation start date
start_date="2015-01-01"

# time step and number of time-steps
dt=1
ndt=$((86*365))

##
function run_elmer() {
   ## define here how to run elmer...
   ## eventually copy scheduler template files and submit
    echo "running Elmer..."
}


