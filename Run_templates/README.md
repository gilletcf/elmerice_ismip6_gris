## Simulation parameters templates:

### Description 

 Template simulation parameter files for MakeRun.sh

### CONTAINS

- [hist.bash](hist.bash): template for a relaxation under constant forcing starting, e.g., from an initial state obtain form inverse methods to bring the modek to the 2015 initial state for the projections
- [ctrl.bash](ctrl.bash): template for a ctrl simulation under constant forcing starting in 2015 and finishing in 2100
- [proj.bash](proj.bash): template to run an ISMIP6 Standard experiement with prescribed aSMB,dSMBdz and front retreat starting in 2015 and finishing in 2100
- [MakeProjs.sh](MakeProjs.sh): a script that will create projection simulations from the list of front retreat forcings available under [CONFIG\_DB]/FrontRetreats/[CONFIG\_NAME]\_[MESH\_NAME]
