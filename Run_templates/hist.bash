#!/bin/bash

sim_name="hist"

# simulation type; constant forcing
sim_type="ctrl" 

# restart file; e.g. initfile in DB
restart_file="${CONFIG_DIR}/CONFIG_DB/init.nc" 

# simulation start date
start_date="1995-01-01" 

# time step and number of time-steps
dt=1 
ndt=$((20*365)) 


##
function run_elmer() {
	## define here how to run elmer...
	## eventually copy scheduler template files and submit
	echo "running Elmer..."
}
