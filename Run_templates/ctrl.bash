#!/bin/bash
sim_name="ctrl"

# simulation type; constant forcing
sim_type="ctrl" 

# restart file; e.g. restart the hist run.
restart_file="${CONFIG_DIR}/hist/restart_hist_1.nc" 

# simulation start date
start_date="2015-01-01" 

# time step and number of time-steps
dt=1 
ndt=$((86*365)) 

##
function run_elmer() {
	## define here how to run elmer...
	## eventually copy scheduler template files and submit
	echo "running Elmer..."

}
