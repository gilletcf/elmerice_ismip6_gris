#!/bin/bash

source Config_parameters.bash

# simulation type; constant forcing
sim_type="proj"

# restart file; e.g. restart the hist run.
restart_file="${CONFIG_DIR}/hist/restart_hist_1.nc"

# the reference elevation for smb elavation feedbaack
ZS_REF_FILE="${CONFIG_DIR}/hist/restart_hist_1.nc"

# simulation start date
start_date="2015-01-01"

# time step and number of time-steps
dt=1
ndt=$((86*365))

##
function run_elmer() {
	## define here how to run elmer...
	## eventually copy scheduler template files and submit
	echo "running Elmer..."
}


test=$(ls ${CONFIG_DIR}/CONFIG_DB/FrontRetreats/${CONFIG_NAME}_${MESH_NAME}/*)

for R_FILE in $test
do

   fname=$(basename $R_FILE)
   echo "- file $fname"

   RCM=$(echo "$fname" | sed 's/_.*//')

   GCM=$(grep -oP "${RCM}_\K.*?(?=-Rhigh|-Rlow|-Rmed)" <<< "$fname")

   R=$(grep -oP "${RCM}_${GCM}-\K.*?(?=_elmer)" <<< "$fname")

   echo "	- RCM=${RCM}"
   echo "	- GCM=${GCM}"
   echo "	- retreat=${R}" 

   aSMB_FILE="${CONFIG_DIR}/CONFIG_DB/SMB_FORCINGS/${MESH_NAME}/aSMB_${GCM}_${RCM}_elmer.nc"
   if [ ! -f "$aSMB_FILE" ]; then
    echo "$aSMB_FILE does not exist."
    exit 42
   fi

   dSMBdz_FILE="${CONFIG_DIR}/CONFIG_DB/SMB_FORCINGS/${MESH_NAME}/dSMBdz_${GCM}_${RCM}_elmer.nc"
   if [ ! -f "$dSMBdz_FILE" ]; then
    echo "$dSMBdz_FILE does not exist."
    exit 42
   fi

   echo $R_FILE

   sim_name=${RCM}_${GCM}-${R}

   source MakeRun.sh

done
