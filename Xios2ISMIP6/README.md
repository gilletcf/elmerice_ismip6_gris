## ISMIP6 post-processing

Tools to process the model outputs produced by XIOS to the required ISMIP6 format.

List of variables produced for a projection from 2015 to 2100 and interpolated on the regular 5km grid:

| *var\_name* | *standard\_name* | *dimension* | *units* |  
|-----------|----------------|-----------|--------|
| acabf | land\_ice\_surface\_specific\_mass\_balance\_flux | (time=86,y=577,x=337) | kg m-2 s-1 |  
| base | base\_altitude | (time=86,y=577,x=337) | m |  
| dlithkdt | tendency\_of\_land\_ice\_thickness | (time=86,y=577,x=337) | m s-1 |  
| iareafl | floating\_ice\_shelf\_area | (time=86) | m2 |  
| iareagr | grounded\_ice\_sheet\_area | (time=86) | m2 |  
| libmassbffl | land\_ice\_basal\_specific\_mass\_balance\_flux | (time=86,y=577,x=337) | kg m-2 s-1 |  
| libmassbfgr | land\_ice\_basal\_specific\_mass\_balance\_flux | (time=86,y=577,x=337) | kg m-2 s-1 |  
| lifmassbf | land\_ice\_specific\_mass\_flux\_due\_to\_calving\_and\_ice\_front\_melting | (time=86,y=577,x=337) | kg m-2 s-1 |  
| ligroundf | land\_ice\_specific\_mass\_flux\_at\_grounding\_line | (time=86,y=577,x=337) | kg m-2 s-1 |  
| lim | land\_ice\_mass | (time=86) | kg |  
| limnsw | land\_ice\_mass\_not\_displacing\_sea\_water | (time=86) | kg |  
| lithk | land\_ice\_thickness | (time=86,y=577,x=337) | m |  
| lithkaf | heigh\_above\_flotation | (time=86,y=577,x=337) | m |  
| orog | surface\_altitude | (time=86,y=577,x=337) | m |  
| sftflf | floating\_ice\_shelf\_area\_fraction | (time=86,y=577,x=337) | 1 |  
| sftgif | land\_ice\_area\_fraction | (time=86,y=577,x=337) | 1 |  
| sftgrf | grounded\_ice\_sheet\_area\_fraction | (time=86,y=577,x=337) | 1 |  
| strbasemag | land\_ice\_basal\_drag | (time=86,y=577,x=337) | Pa |  
| tendacabf | tendency\_of\_land\_ice\_mass\_due\_to\_surface\_mass\_balance | (time=86) | kg s-1 |  
| tendlibmassbf | tendency\_of\_land\_ice\_mass\_due\_to\_basal\_mass\_balance | (time=86) | kg s-1 |  
| tendlibmassbffl | tendency\_of\_land\_ice\_mass\_due\_to\_basal\_mass\_balance | (time=86) | kg s-1 |  
| tendlifmassbf | tendency\_of\_land\_ice\_mass\_due\_to\_calving\_and\_ice\_front\_melting | (time=86) | kg s-1 |  
| tendligroundf | tendency\_of\_grounded\_ice\_mass | (time=86) | kg s-1 |  
| topg | bedrock\_altitude | (time=86,y=577,x=337) | m |  
| velmean |  | (time=86,y=577,x=337) | m s-1 |  
| xvelmean | land\_ice\_vertical\_mean\_x\_velocity | (time=86,y=577,x=337) | m s-1 |  
| yvelmean | land\_ice\_vertical\_mean\_y\_velocity | (time=86,y=577,x=337) | m s-1 |  


### Contains:  

- [XIOS\_2\_ISMIP6.sh](XIOS_2_ISMIP6.sh): process and interpolate the variables required by ISMIP6  

- [IceMask\_2\_Grid00600m.sh](IceMask_2_Grid00600m.sh):  interpolate the ice fraction sftgif to the 600m-resolution grid using to produce the retreat masks


