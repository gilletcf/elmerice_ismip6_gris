#!/bin/bash
########################################################################################
## Interpolate icy mask sftgif to regular GrIS grid @600m
#  take last time step from the input file provided as first argument
#  OUTPUT:
#	sftgif_${CONFIG_NAME}.nc: the ice fraction on the regular grid @600m.
########################################################################################
# Required arguments:
#	1. input_file that contains the ElmerIce icy mask
#	2. the config. parameters (Config_parameters.bash)
#		- at least need to define CONFIG_DIR,  CONFIG_NAME, and MESH_NAME
#
# Required inputs:
#	${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/mesh_cdo_grid.nc: the mesh grid description file
#	${CONFIG_DIR}/CONFIG_DB/GRID_RETREAT/GrIS_00600m_grid.nc: the regular grid
#
#  Requires:
#	- cdo
#	- nco
#
# Usage:
#	./IceMask_2_Grid00600m.sh ISMIP6_STATE_FILE CONFIG_PARAM FILES
########################################################################################
if [ ! $# -eq 2 ]; then
	echo "provide the input file and config. parameters file as argument"; exit 42
fi
# input data file providsed as first argument
input_file=$1
if [ ! -f "$input_file" ]; then
	echo "input argument is not a valid file; exit"; exit 42
fi
# source config. parameters file 
source $2 || exit 42

########################################################################
# unstructured grid file 
grid_file=${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/mesh_cdo_grid.nc
# regular grid file
grid_reg=${CONFIG_DIR}/CONFIG_DB/GRID_RETREAT/GrIS_00600m_grid.nc
#########################################################################

# var to interpolate and its time axis
var_name=sftgif
time_axis=time_instant

outputfile=${var_name}_${CONFIG_NAME}.nc

TMPf=tmp.nc
## extract the variable (last time index) and time axis associated to it
## extract in classic as problem on mahti, dahu when renaming a variable with a coordiante name......
ncks -O -3 -C -d time,-1 -v $var_name,$time_axis,${time_axis}_bounds  $input_file $TMPf || exit 42
## rename cell dim
ncrename -d nmesh2D_face,ncells -v $time_axis,time $TMPf || exit 42
## change coordinates of var
ncatted -a coordinates,$var_name,o,c,"lon lat" $TMPf || exit 42
## copy coordinates from the grid
ncks -A -v lat,lon,lat_bnds,lon_bnds $grid_file $TMPf || exit 42

echo "RENAMING DONE!!"

## remapping
# fraction areas require destarea as remap norm
export CDO_REMAP_NORM=destarea
cdo remapycon,$grid_reg -selname,$var_name $TMPf $outputfile || exit 42

echo "REMAPPING DONE!!"

rm -f $TMPf || exit 42

echo "ALL DONE"
