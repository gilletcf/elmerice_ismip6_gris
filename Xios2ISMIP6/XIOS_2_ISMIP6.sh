#!/bin/bash
###############################################################################
#  Process unstructured ISMIP6 outputs to Regular grid
#  Outputs are stored under DATA_ISMIP6
#
#  Description:
#    Process ismip6_states, ismip6_fluxes, and ismip6_scalars_true_cell_area files
#    that must be present in the current directory
#    The name of the experiment (that is used to get the file names) must be given as argument
#     or is the name of the current directory
#    ISMIP6 files should be named ismip6_fluxes_${exp_name}_*.nc with exp_name in lower case
#
#    Must be run from a simulation directory with the configuration parameters
#    in ../Config_parameters.bash
#
#    Specific keywords:
#	ISMIP6_GRID= Name of the regular grid; should be under ${CONFIG_DIR}/CONFIG_DB/GRID_ISMIP6/${ISMIP6_GRID}_grid.nc
# 	institution= Name of the institution that will appear in the global attributes
#	contacts=    Contact names that will appear in the global attributes
#
#    Mandatory Keywords:
#	CONFIG_NAME
#	CONFIG_DIR
#	MESH_NAME
#    
#
#  Requires:
#	- cdo
#	- nco
#
#  usage:
#       XIOS_2_ISMIP6.sh EXP_NAME
################################################################################

check_file_exist(){
  if [ ! -f "$1" ]; then
  	echo "file $1 does not exist; exit" ; exit 42
  fi
}

set_parameters() {

  source ../Config_parameters.bash || exit 42
  
  ISNAME=GrIS
  MODEL=${CONFIG_NAME}

 ## output grid
  if [ -z "$ISMIP6_GRID" ]; then
  	echo "ISMIP6_GRID is not set; exit" ; exit 42
  fi
  GRIDOUT=${CONFIG_DIR}/CONFIG_DB/GRID_ISMIP6/${ISMIP6_GRID}_grid.nc
  check_file_exist $GRIDOUT

 ## input grid and interpolation weights
  GRIDIN=${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/mesh_cdo_grid.nc
  check_file_exist $GRIDIN

  WEIGHTS_destarea=${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/toISMIP6/ycon_weights_mesh_to_${ISMIP6_GRID}_destarea.nc
  if [ ! -f "$WEIGHTS_destarea" ]; then
  	 echo "interpolation weights ${WEIGHTS_destarea} does not exist; create it"
         mkdir -p ${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/toISMIP6 || exit 42
	 export CDO_REMAP_NORM=destarea
         cdo genycon,$GRIDOUT $GRIDIN $WEIGHTS_destarea || exit 42
  fi

  WEIGHTS_fracarea=${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/toISMIP6/ycon_weights_mesh_to_${ISMIP6_GRID}_fracarea.nc
  if [ ! -f "$WEIGHTS_fracarea" ]; then
  	 echo "interpolation weights ${WEIGHTS_fracarea} does not exist; create it"
         mkdir -p ${CONFIG_DIR}/CONFIG_DB/${MESH_NAME}/toISMIP6 || exit 42
	 export CDO_REMAP_NORM=fracarea
         cdo genycon,$GRIDOUT $GRIDIN $WEIGHTS_fracarea || exit 42
  fi

}

join_arr() {
  local IFS="$1"
  shift
  echo "$*"
}

compute_interpolation() {
   echo "####################################################################################"
   echo "  remap:  ${var_list[@]}"
   echo "  from file $FILEIN"
   echo "  from grid $GRIDIN to $GRIDOUT"
   echo "  using CDO_REMAP_NORM=$CDO_REMAP_NORM"
   echo "  using WEIGHTS $WEIGHTS"
   echo "-----"

   ## extract var_list
   TMPf=tmpf.nc
   ncks -O -3 -C -v $time_axis,${time_axis}_bounds,$var_list_names $FILEIN $TMPf || exit 42

   ## rename cell dim
   ncrename -d nmesh2D_face,ncells $TMPf || exit 42

   ## change coordinates of var
   ## sanity check time_axis
   for VAR in "${var_list[@]}"
   do
      taxis=`ncdump -h $TMPf | grep -P "\t$VAR:coordinates" | egrep -o '(time)\w+'`
      if [ ! "$time_axis" == "$taxis" ]; then echo "time_axis $taxis for $VAR incorrect"; exit 42  ; fi
      ncatted -a coordinates,$VAR,o,c,"lon lat" $TMPf || exit 42
   done

   ncks -A -v lat,lon,lat_bnds,lon_bnds $GRIDIN $TMPf || exit 42

   echo "   RENAMING DONE!!"

  ## remapping
  TMPf1=tmpf1.nc
  cdo remap,$GRIDOUT,$WEIGHTS -selname,$var_list_names $TMPf $TMPf1 || exit 42

  TMPf2=tmpf2.nc
  cdo setmissval,1e20 $TMPf1 $TMPf2 || exit 42

  echo "   REMAPING DONE!!"

  # fix att, name ...
  ncrename -d nv4,nv -d $time_axis,time -v $time_axis,time $TMPf2 || exit 42
  ncks -A -v mapping $GRIDOUT $TMPf2                  || exit 42

  for VAR in "${var_list[@]}"
  do

    ncatted -a 'grid_mapping',$VAR,c,c,'mapping' $TMPf2 || exit 42
    ncatted -a 'cdo_remap_norm',$VAR,c,c,"$CDO_REMAP_NORM" $TMPf2 || exit 42
    ncatted -a 'mesh',$VAR,d,,                   $TMPf2 || exit 42
    ncatted -a 'location',$VAR,d,,               $TMPf2 || exit 42
    ncatted -a 'missing_value',$VAR,d,,          $TMPf2 || exit 42

    FILEOUT=${WRKDIR}/DATA_ISMIP6/${VAR}_${ISNAME}_${MODEL}_${EXP}.nc

    ncks -O -4 -v $VAR  --dfl_lvl 1 --cnk_dmn x,200 --cnk_dmn y,200 --cnk_dmn time,1 $TMPf2 $FILEOUT || exit 42
 
    fix_global_attributes 
  done

  rm -f $TMPf $TMPf1 $TMPf2

  echo "####################################################################################"
  echo "  DONE "
  echo "####################################################################################"
}

process_scalars(){

  for VAR in lim limnsw iareagr iareafl tendacabf tendlibmassbf tendlibmassbffl tendlifmassbf tendligroundf ;  do

      FILEOUT=${WRKDIR}/DATA_ISMIP6/${VAR}_${ISNAME}_${MODEL}_${EXP}.nc

      echo ""
      echo "Extract $VAR ..."
      echo " from $FILEIN to $FILEOUT "
      echo ""
      VAR_TCA=${VAR}_tca

      time_axis=`ncdump -h $FILEIN | grep -P "\t${VAR_TCA}:coordinates" | egrep -o '(time)\w+'`
      if [ -z "$time_axis" ];then echo "time_axis is empty; exit"; exit 42 ; fi
      ncks -O -C -v ${VAR_TCA},$time_axis,${time_axis}_bounds $FILEIN $FILEOUT
      ncrename -v ${VAR_TCA},${VAR} -v $time_axis,time $FILEOUT
      ncatted -a 'coordinates',$VAR,d,, $FILEOUT

      fix_global_attributes
  done
}

fix_global_attributes(){
    ncatted -a uuid,global,d,, $FILEOUT || exit 42
    ncatted -a timeStamp,global,d,, $FILEOUT || exit 42
    ncatted -a Conventions,global,d,, $FILEOUT || exit 42
    ncatted -a title,global,d,, $FILEOUT || exit 42
    ncatted -a description,global,d,, $FILEOUT || exit 42
    ncatted -a name,global,d,, $FILEOUT || exit 42
    ncatted -a projection,global,d,, $FILEOUT || exit 42
    ncatted -a CDI,global,d,, -a CDO,global,d,, $FILEOUT || exit 42

    ncatted -a title,global,a,c,"ISMIP6 GrIS simulation : variable $VAR for experiment $EXP" $FILEOUT || exit 42
    ncatted -a url,global,a,c,"https://www.climate-cryosphere.org/wiki/index.php?title=ISMIP6-Projections-Greenland" $FILEOUT || exit 42
    ncatted -a experiment,global,a,c,"$EXP" $FILEOUT || exit 42
    ncatted -a model_configuration,global,a,c,"$MODEL" $FILEOUT || exit 42
    if [ ! -z "$institution" ]; then
    	ncatted -a institution,global,a,c,"$institution" $FILEOUT || exit 42
    fi
    if [ ! -z "$contacts" ]; then
        ncatted -a contacts,global,a,c,"$contacts" $FILEOUT || exit 42
    fi

    ncatted -a history_of_appended_files,global,d,, $FILEOUT || exit 42
    ncatted -h -a history,global,d,, -a NCO,global,d,, $FILEOUT || exit 42
}

concatenate_results(){
# concatanate all the files for scalar, fluxes and states
   if [ -f $fluxes_file ]; then rm $fluxes_file ; fi

   files=$(ls ismip6_fluxes_${cexp}_?.nc) || exit 42
   nn=$(echo $files | wc -w)
   if [ "$nn" -gt "1" ]; then
     ncrcat -O $files $fluxes_file || exit 42
   else
     cp ismip6_fluxes_${cexp}_1.nc $fluxes_file || exit 42
   fi

   if [ -f $states_file ]; then rm $states_file ; fi

   files=$(ls ismip6_states_${cexp}_?.nc) || exit 42
   nn=$(echo $files | wc -w)
   if [ "$nn" -gt "1" ]; then
     ncrcat -O $files $states_file || exit 42
   else
     cp ismip6_states_${cexp}_1.nc $states_file || exit 42
   fi 

   if [ -f $scalars_file ]; then rm $scalars_file ; fi
   
   files=$(ls ismip6_scalars_true_cell_area_${cexp}_?.nc) || exit 42
   nn=$(echo $files | wc -w)
   if [ "$nn" -gt "1" ]; then
     ncrcat -O $files $scalars_file || exit 42
   else
     cp ismip6_scalars_true_cell_area_${cexp}_1.nc $scalars_file || exit 42
   fi
}

## Check that the exp_name is provided as argument
if [ -z "$1" ]; then
	EXP=$(basename $PWD)
else
	EXP=$1
fi

echo "WORKING IN : $PWD"
WRKDIR=$PWD
echo "Processing experiment $EXP"
echo "number of threads :" $OMP_NUM_THREADS

## set generic parameters
set_parameters

## create tmp and output dirs
if [ -d ${WRKDIR}/DATA_ISMIP6 ]; then echo "directory DATA_ISMIP6 already exist; return"; exit 42 ; fi
mkdir -p ${WRKDIR}/DATA_ISMIP6

mkdir -p ${WRKDIR}/TMPDIR


## concatenate/copy output files...
cexp=`echo "$EXP" | tr '[:upper:]' '[:lower:]'` || nerr=$((nerr+1))
states_file=${WRKDIR}/TMPDIR/ismip6_states_${cexp}.nc
fluxes_file=${WRKDIR}/TMPDIR/ismip6_fluxes_${cexp}.nc
scalars_file=${WRKDIR}/TMPDIR/ismip6_scalars_true_cell_area_${cexp}.nc

concatenate_results

## run the interpolation
cd ${WRKDIR}/TMPDIR

## process scalars
FILEIN=$scalars_file
process_scalars


## process 2D files... 
# -> use fracarea for states
export CDO_REMAP_NORM='fracarea'
WEIGHTS=$WEIGHTS_fracarea

### state vars
unset var_list var_list_names time_axis
var_list=("lithk" "orog" "base" "topg" "xvelmean" "yvelmean" "strbasemag" "lithkaf" "velmean")
var_list_names=$(join_arr , "${var_list[@]}")
time_axis="time_instant"

FILEIN=$states_file
compute_interpolation

### flux vars
# -> use destarea for fluxes
export CDO_REMAP_NORM='destarea'
WEIGHTS=$WEIGHTS_destarea

unset var_list var_list_names time_axis
var_list=("acabf" "libmassbfgr" "libmassbffl" "dlithkdt" "lifmassbf" "ligroundf")
var_list_names=$(join_arr , "${var_list[@]}")
time_axis="time_centered" 

FILEIN=$fluxes_file
compute_interpolation

## ice fractions 
# -> use destarea for ice fractions
export CDO_REMAP_NORM='destarea'
WEIGHTS=$WEIGHTS_destarea

var_list=("sftgif" "sftgrf" "sftflf")
var_list_names=$(join_arr , "${var_list[@]}")
time_axis="time_instant"

FILEIN=$states_file
compute_interpolation

